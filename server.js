var http = require('http');
var UAParser = require('ua-parser-js');

function getOs(req) {
    var parser = new UAParser();
    var os = parser.setUA(req.headers['user-agent']).getOS();
    return os.name + ' ' + os.version;
}

var server = http.createServer(function(req, res) {
    var ip = req.connection.remoteAddress.split(':').pop();
    var language = req.headers['accept-language'];
    var os = getOs(req);
    var result = {
        ip: ip,
        language: language,
        os: os
    };

    res.setHeader('Content-type', 'application/json');
    res.end(JSON.stringify(result));
});

server.listen(process.env.PORT || 3000);